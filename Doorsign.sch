EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+BATT #PWR0101
U 1 1 60039D5A
P 9450 1800
F 0 "#PWR0101" H 9450 1650 50  0001 C CNN
F 1 "+BATT" H 9465 1973 50  0000 C CNN
F 2 "" H 9450 1800 50  0001 C CNN
F 3 "" H 9450 1800 50  0001 C CNN
	1    9450 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6003A4B2
P 9450 2100
F 0 "#PWR0102" H 9450 1850 50  0001 C CNN
F 1 "GND" H 9455 1927 50  0000 C CNN
F 2 "" H 9450 2100 50  0001 C CNN
F 3 "" H 9450 2100 50  0001 C CNN
	1    9450 2100
	1    0    0    -1  
$EndComp
Text Notes 8900 4500 0    50   ~ 0
Input: 1 cell Li-Po battery @ 220 mAh\nL402036 CELLEVIA
Text Notes 7400 2050 0    100  ~ 0
Power
Text Notes 1000 1550 0    100  ~ 0
MCU
$Sheet
S 4400 1550 1050 800 
U 6004A299
F0 "LEDs" 50
F1 "LEDs.sch" 50
F2 "DIN" I L 4400 1950 50 
$EndSheet
$Comp
L power:+BATT #PWR0103
U 1 1 6007C2A8
P 8800 1800
F 0 "#PWR0103" H 8800 1650 50  0001 C CNN
F 1 "+BATT" H 8815 1973 50  0000 C CNN
F 2 "" H 8800 1800 50  0001 C CNN
F 3 "" H 8800 1800 50  0001 C CNN
	1    8800 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 6007C5AC
P 8800 2100
F 0 "#PWR0104" H 8800 1850 50  0001 C CNN
F 1 "GND" H 8805 1927 50  0000 C CNN
F 2 "" H 8800 2100 50  0001 C CNN
F 3 "" H 8800 2100 50  0001 C CNN
	1    8800 2100
	1    0    0    -1  
$EndComp
$Sheet
S 4400 3700 1050 800 
U 600B9BE2
F0 "PIR" 50
F1 "PIR.sch" 50
F2 "PIR_HI" O L 4400 4250 50 
F3 "PIR_LO" O L 4400 3950 50 
$EndSheet
$Comp
L Connector:USB_B_Micro J2
U 1 1 600DE9C7
P 7650 2950
F 0 "J2" H 7707 3507 50  0000 C CNN
F 1 "USB_B_Micro" H 7707 3416 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex_47346-0001" H 7800 2900 50  0001 C CNN
F 3 "~" H 7800 2900 50  0001 C CNN
F 4 "47346-0001" H 7707 3325 50  0000 C CNN "MPN"
F 5 "Farnell" H 7650 2950 50  0001 C CNN "Source"
F 6 "1568026" H 7650 2950 50  0001 C CNN "Farnell No."
	1    7650 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 600E09B8
P 7600 3450
F 0 "#PWR0105" H 7600 3200 50  0001 C CNN
F 1 "GND" H 7605 3277 50  0000 C CNN
F 2 "" H 7600 3450 50  0001 C CNN
F 3 "" H 7600 3450 50  0001 C CNN
	1    7600 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3350 7550 3400
Wire Wire Line
	7550 3400 7600 3400
Wire Wire Line
	7600 3400 7600 3450
Wire Wire Line
	7650 3350 7650 3400
Wire Wire Line
	7650 3400 7600 3400
Connection ~ 7600 3400
$Comp
L MCU_Microchip_ATmega:ATmega88-20AU U1
U 1 1 600E1BFA
P 1800 3250
F 0 "U1" H 2200 4700 50  0000 C CNN
F 1 "ATmega88-20AU" H 2250 1750 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 1800 3250 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2545-8-bit-AVR-Microcontroller-ATmega48-88-168_Datasheet.pdf" H 1800 3250 50  0001 C CNN
F 4 "ATmega88-20AU" H 1800 3250 50  0001 C CNN "MPN"
F 5 "ETA" H 1800 3250 50  0001 C CNN "Source"
	1    1800 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector:AVR-ISP-6 J1
U 1 1 600E3A33
P 1700 6450
F 0 "J1" H 1371 6546 50  0000 R CNN
F 1 "AVR-ISP-6" H 1371 6455 50  0000 R CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Vertical" V 1450 6500 50  0001 C CNN
F 3 " ~" H 425 5900 50  0001 C CNN
F 4 " ~" H 1700 6450 50  0001 C CNN "MPN"
F 5 " ~" H 1700 6450 50  0001 C CNN "Source"
F 6 " ~" H 1700 6450 50  0001 C CNN "Farnell No."
	1    1700 6450
	1    0    0    -1  
$EndComp
Text Notes 1000 5600 0    100  ~ 0
AVR ISP Programmer
$Comp
L power:+BATT #PWR0106
U 1 1 600E7FEB
P 1850 1700
F 0 "#PWR0106" H 1850 1550 50  0001 C CNN
F 1 "+BATT" H 1865 1873 50  0000 C CNN
F 2 "" H 1850 1700 50  0001 C CNN
F 3 "" H 1850 1700 50  0001 C CNN
	1    1850 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 1700 1800 1700
Wire Wire Line
	1800 1700 1800 1750
Wire Wire Line
	1850 1700 1900 1700
Wire Wire Line
	1900 1700 1900 1750
Connection ~ 1850 1700
Text Notes 8900 4250 0    50   ~ 0
Charge current = 200mA, i.e. approx 1C
$Comp
L Battery_Management:MCP73831-2-OT U2
U 1 1 600EADF6
P 9650 3250
F 0 "U2" H 9850 3550 50  0000 C CNN
F 1 "MCP73831-2-OT" H 10150 2950 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 9700 3000 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 9500 3200 50  0001 C CNN
F 4 "MCP73831-2-OT" H 9650 3250 50  0001 C CNN "MPN"
F 5 "Farnell" H 9650 3250 50  0001 C CNN "Source"
F 6 "1332158" H 9650 3250 50  0001 C CNN "Farnell No."
	1    9650 3250
	1    0    0    -1  
$EndComp
NoConn ~ 7950 2950
NoConn ~ 7950 3050
NoConn ~ 7950 3150
$Comp
L Device:D_Schottky D1
U 1 1 600EED0E
P 8300 2750
F 0 "D1" H 8300 2533 50  0000 C CNN
F 1 "MBRA140" H 8300 2624 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 8300 2750 50  0001 C CNN
F 3 "~" H 8300 2750 50  0001 C CNN
F 4 "MBRA140" H 8300 2750 50  0001 C CNN "MPN"
F 5 "Farnell" H 8300 2750 50  0001 C CNN "Source"
F 6 "8660417" H 8300 2750 50  0001 C CNN "Farnell No."
	1    8300 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	7950 2750 8150 2750
Wire Wire Line
	8450 2750 8550 2750
$Comp
L power:GND #PWR0107
U 1 1 600F52B1
P 9650 3750
F 0 "#PWR0107" H 9650 3500 50  0001 C CNN
F 1 "GND" H 9655 3577 50  0000 C CNN
F 2 "" H 9650 3750 50  0001 C CNN
F 3 "" H 9650 3750 50  0001 C CNN
	1    9650 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 600F5A44
P 9050 3750
F 0 "#PWR0108" H 9050 3500 50  0001 C CNN
F 1 "GND" H 9055 3577 50  0000 C CNN
F 2 "" H 9050 3750 50  0001 C CNN
F 3 "" H 9050 3750 50  0001 C CNN
	1    9050 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 3450 9050 3350
Wire Wire Line
	9050 3350 9250 3350
Wire Wire Line
	9650 3550 9650 3750
$Comp
L Device:C C4
U 1 1 600F738C
P 8550 3000
F 0 "C4" H 8665 3046 50  0000 L CNN
F 1 "4.7u" H 8665 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8588 2850 50  0001 C CNN
F 3 "~" H 8550 3000 50  0001 C CNN
F 4 "" H 8550 3000 50  0001 C CNN "MPN"
F 5 "ETA" H 8550 3000 50  0001 C CNN "Source"
F 6 "" H 8550 3000 50  0001 C CNN "Farnell No."
	1    8550 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 600F9F58
P 8550 3150
F 0 "#PWR0109" H 8550 2900 50  0001 C CNN
F 1 "GND" H 8555 2977 50  0000 C CNN
F 2 "" H 8550 3150 50  0001 C CNN
F 3 "" H 8550 3150 50  0001 C CNN
	1    8550 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 2850 8550 2750
Connection ~ 8550 2750
Wire Wire Line
	9650 2750 9650 2950
Wire Wire Line
	8550 2750 9650 2750
$Comp
L Device:LED D2
U 1 1 600FC117
P 9900 2750
F 0 "D2" H 9893 2495 50  0000 C CNN
F 1 "Red" H 9893 2586 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric" H 9900 2750 50  0001 C CNN
F 3 "~" H 9900 2750 50  0001 C CNN
F 4 "" H 9900 2750 50  0001 C CNN "MPN"
F 5 "ETA" H 9900 2750 50  0001 C CNN "Source"
	1    9900 2750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 600FF20F
P 9050 3600
F 0 "R2" H 9120 3646 50  0000 L CNN
F 1 "5k" H 9120 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8980 3600 50  0001 C CNN
F 3 "~" H 9050 3600 50  0001 C CNN
F 4 "CPF0603F4K99C1" H 9050 3600 50  0001 C CNN "MPN"
F 5 "Farnell" H 9050 3600 50  0001 C CNN "Source"
F 6 "1527548" H 9050 3600 50  0001 C CNN "Farnell No."
	1    9050 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 600FF666
P 10300 2750
F 0 "R3" V 10093 2750 50  0000 C CNN
F 1 "470" V 10184 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10230 2750 50  0001 C CNN
F 3 "~" H 10300 2750 50  0001 C CNN
F 4 "" H 10300 2750 50  0001 C CNN "MPN"
F 5 "ETA" H 10300 2750 50  0001 C CNN "Source"
F 6 "" H 10300 2750 50  0001 C CNN "Farnell No."
	1    10300 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 2750 9650 2750
Connection ~ 9650 2750
Wire Wire Line
	10050 2750 10150 2750
Wire Wire Line
	10050 3350 10550 3350
Wire Wire Line
	10550 3350 10550 2750
Wire Wire Line
	10550 2750 10450 2750
$Comp
L power:+BATT #PWR0110
U 1 1 60100B6E
P 10150 3100
F 0 "#PWR0110" H 10150 2950 50  0001 C CNN
F 1 "+BATT" H 10165 3273 50  0000 C CNN
F 2 "" H 10150 3100 50  0001 C CNN
F 3 "" H 10150 3100 50  0001 C CNN
	1    10150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 3100 10150 3150
Wire Wire Line
	10150 3150 10050 3150
$Comp
L Device:C C3
U 1 1 6010BAAF
P 8300 1950
F 0 "C3" H 8415 1996 50  0000 L CNN
F 1 "4.7u" H 8415 1905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8338 1800 50  0001 C CNN
F 3 "~" H 8300 1950 50  0001 C CNN
F 4 "" H 8300 1950 50  0001 C CNN "MPN"
F 5 "ETA" H 8300 1950 50  0001 C CNN "Source"
F 6 "" H 8300 1950 50  0001 C CNN "Farnell No."
	1    8300 1950
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR0111
U 1 1 6010C41D
P 8300 1800
F 0 "#PWR0111" H 8300 1650 50  0001 C CNN
F 1 "+BATT" H 8315 1973 50  0000 C CNN
F 2 "" H 8300 1800 50  0001 C CNN
F 3 "" H 8300 1800 50  0001 C CNN
	1    8300 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 6010C7A9
P 8300 2100
F 0 "#PWR0112" H 8300 1850 50  0001 C CNN
F 1 "GND" H 8305 1927 50  0000 C CNN
F 2 "" H 8300 2100 50  0001 C CNN
F 3 "" H 8300 2100 50  0001 C CNN
	1    8300 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 6011C518
P 1800 4750
F 0 "#PWR0113" H 1800 4500 50  0001 C CNN
F 1 "GND" H 1805 4577 50  0000 C CNN
F 2 "" H 1800 4750 50  0001 C CNN
F 3 "" H 1800 4750 50  0001 C CNN
	1    1800 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 6011CF5F
P 1600 6850
F 0 "#PWR0114" H 1600 6600 50  0001 C CNN
F 1 "GND" H 1605 6677 50  0000 C CNN
F 2 "" H 1600 6850 50  0001 C CNN
F 3 "" H 1600 6850 50  0001 C CNN
	1    1600 6850
	1    0    0    -1  
$EndComp
Text Label 2200 6350 0    50   ~ 0
MOSI
Text Label 2200 6250 0    50   ~ 0
MISO
Wire Wire Line
	2100 6250 2200 6250
Wire Wire Line
	2200 6350 2100 6350
Wire Wire Line
	2200 6450 2100 6450
Wire Wire Line
	2100 6550 2200 6550
Text Label 2200 6550 0    50   ~ 0
~RST
Text Label 2200 6450 0    50   ~ 0
SCK
Text Label 2500 2350 0    50   ~ 0
MOSI
Text Label 2500 2450 0    50   ~ 0
MISO
Wire Wire Line
	2400 2450 2500 2450
Wire Wire Line
	2500 2350 2400 2350
Wire Wire Line
	2500 2550 2400 2550
Text Label 3200 3850 0    50   ~ 0
~RST
Text Label 2500 2550 0    50   ~ 0
SCK
$Comp
L Device:R R1
U 1 1 601232B6
P 3100 3600
F 0 "R1" H 3030 3554 50  0000 R CNN
F 1 "10k" H 3030 3645 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3030 3600 50  0001 C CNN
F 3 "~" H 3100 3600 50  0001 C CNN
F 4 "CRCW060310K0JNEA" H 3100 3600 50  0001 C CNN "MPN"
F 5 "Farnell" H 3100 3600 50  0001 C CNN "Source"
F 6 "1469749" H 3100 3600 50  0001 C CNN "Farnell No."
	1    3100 3600
	-1   0    0    1   
$EndComp
$Comp
L power:+BATT #PWR0115
U 1 1 60123DC5
P 3100 3400
F 0 "#PWR0115" H 3100 3250 50  0001 C CNN
F 1 "+BATT" H 3115 3573 50  0000 C CNN
F 2 "" H 3100 3400 50  0001 C CNN
F 3 "" H 3100 3400 50  0001 C CNN
	1    3100 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3400 3100 3450
Wire Wire Line
	3100 3750 3100 3850
Wire Wire Line
	3100 3850 3200 3850
$Comp
L Device:Crystal_GND24_Small Y1
U 1 1 6012B1C1
P 3250 2700
F 0 "Y1" V 3150 2750 50  0000 L CNN
F 1 "16MHz" V 3350 2750 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_Abracon_ABM3B-4Pin_5.0x3.2mm" H 3250 2700 50  0001 C CNN
F 3 "~" H 3250 2700 50  0001 C CNN
F 4 "ABM3B-16.000MHZ-B2-T" H 3250 2700 50  0001 C CNN "MPN"
F 5 "Farnell" H 3250 2700 50  0001 C CNN "Source"
F 6 "1611813" H 3250 2700 50  0001 C CNN "Farnell No."
	1    3250 2700
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 6012BC1E
P 3500 2450
F 0 "C1" V 3248 2450 50  0000 C CNN
F 1 "18p" V 3339 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3538 2300 50  0001 C CNN
F 3 "~" H 3500 2450 50  0001 C CNN
F 4 "C0603C180J5GACTU" H 3500 2450 50  0001 C CNN "MPN"
F 5 "Farnell" H 3500 2450 50  0001 C CNN "Source"
F 6 "1414620" H 3500 2450 50  0001 C CNN "Farnell No."
	1    3500 2450
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 6012C087
P 3500 2950
F 0 "C2" V 3660 2950 50  0000 C CNN
F 1 "18p" V 3751 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3538 2800 50  0001 C CNN
F 3 "~" H 3500 2950 50  0001 C CNN
F 4 "C0603C180J5GACTU" H 3500 2950 50  0001 C CNN "MPN"
F 5 "Farnell" H 3500 2950 50  0001 C CNN "Source"
F 6 "1414620" H 3500 2950 50  0001 C CNN "Farnell No."
	1    3500 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	2400 3550 2700 3550
Wire Wire Line
	2700 3550 2700 3850
Wire Wire Line
	2700 3850 3100 3850
Connection ~ 3100 3850
Wire Wire Line
	2400 2650 2900 2650
Wire Wire Line
	2400 2750 2900 2750
Wire Wire Line
	3250 2800 3250 2950
Connection ~ 3250 2950
Wire Wire Line
	3250 2950 3350 2950
Wire Wire Line
	3250 2600 3250 2450
Connection ~ 3250 2450
Wire Wire Line
	3250 2450 3350 2450
$Comp
L power:GND #PWR0116
U 1 1 6013085B
P 3800 3050
F 0 "#PWR0116" H 3800 2800 50  0001 C CNN
F 1 "GND" H 3805 2877 50  0000 C CNN
F 2 "" H 3800 3050 50  0001 C CNN
F 3 "" H 3800 3050 50  0001 C CNN
	1    3800 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2450 3800 2450
Wire Wire Line
	3800 2450 3800 2950
Wire Wire Line
	3650 2950 3800 2950
Connection ~ 3800 2950
Wire Wire Line
	3800 2950 3800 3050
$Comp
L power:+BATT #PWR0117
U 1 1 60139C01
P 1600 5950
F 0 "#PWR0117" H 1600 5800 50  0001 C CNN
F 1 "+BATT" H 1615 6123 50  0000 C CNN
F 2 "" H 1600 5950 50  0001 C CNN
F 3 "" H 1600 5950 50  0001 C CNN
	1    1600 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 4150 4300 4150
Wire Wire Line
	2400 4050 4300 4050
Wire Wire Line
	4300 4050 4300 3950
Wire Wire Line
	4300 3950 4400 3950
Wire Wire Line
	4300 4250 4400 4250
Wire Wire Line
	4300 4150 4300 4250
Wire Wire Line
	3000 1950 3000 2150
Wire Wire Line
	3000 1950 4400 1950
Wire Wire Line
	2400 2150 3000 2150
$Comp
L Device:CP CP1
U 1 1 6014E8D9
P 8800 1950
AR Path="/6014E8D9" Ref="CP1"  Part="1" 
AR Path="/600B9BE2/6014E8D9" Ref="C?"  Part="1" 
F 0 "CP1" H 8918 1996 50  0000 L CNN
F 1 "100u" H 8918 1905 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.9" H 8838 1800 50  0001 C CNN
F 3 "~" H 8800 1950 50  0001 C CNN
F 4 "EEE0JA101SP" H 8800 1950 50  0001 C CNN "MPN"
F 5 "Farnell" H 8800 1950 50  0001 C CNN "Source"
F 6 "9696903" H 8800 1950 50  0001 C CNN "Farnell No."
	1    8800 1950
	1    0    0    -1  
$EndComp
$Comp
L S2B-PH-SM4-TB_LF__SN_:S2B-PH-SM4-TB(LF)(SN) J3
U 1 1 60161DDD
P 10000 1900
F 0 "J3" H 10228 1941 50  0000 L CNN
F 1 "Battery connector" H 10228 1850 50  0000 L CNN
F 2 "JST_S2B-PH-SM4-TB(LF)(SN)" H 10000 1900 50  0001 L BNN
F 3 "" H 10000 1900 50  0001 L BNN
F 4 "Manufacturer Recommendation" H 10000 1900 50  0001 L BNN "STANDARD"
F 5 "" H 10000 1900 50  0001 L BNN "MANUFACTURER"
F 6 "S2B-PH-SM4-TB" H 10228 1759 50  0000 L CNN "MPN"
F 7 "TME" H 10000 1900 50  0001 C CNN "Source"
	1    10000 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 1800 9700 1800
Wire Wire Line
	9450 2100 9450 2000
Wire Wire Line
	9600 2100 9600 2000
Wire Wire Line
	9600 2100 9700 2100
Wire Wire Line
	9600 1900 9700 1900
Wire Wire Line
	9600 2000 9450 2000
Connection ~ 9600 2000
Wire Wire Line
	9600 2000 9600 1900
$Comp
L power:GND #PWR0138
U 1 1 600DB3E5
P 3350 2700
F 0 "#PWR0138" H 3350 2450 50  0001 C CNN
F 1 "GND" H 3355 2527 50  0000 C CNN
F 2 "" H 3350 2700 50  0001 C CNN
F 3 "" H 3350 2700 50  0001 C CNN
	1    3350 2700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0139
U 1 1 600DB8B4
P 3150 2700
F 0 "#PWR0139" H 3150 2450 50  0001 C CNN
F 1 "GND" H 3155 2527 50  0000 C CNN
F 2 "" H 3150 2700 50  0001 C CNN
F 3 "" H 3150 2700 50  0001 C CNN
	1    3150 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	2900 2450 2900 2650
Wire Wire Line
	2900 2450 3250 2450
Wire Wire Line
	2900 2750 2900 2950
Wire Wire Line
	2900 2950 3250 2950
$Comp
L power:+BATT #PWR0140
U 1 1 6056FA55
P 9700 1150
F 0 "#PWR0140" H 9700 1000 50  0001 C CNN
F 1 "+BATT" H 9715 1323 50  0000 C CNN
F 2 "" H 9700 1150 50  0001 C CNN
F 3 "" H 9700 1150 50  0001 C CNN
	1    9700 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1150 9950 1150
$Comp
L power:GND #PWR0141
U 1 1 605710B9
P 9700 1250
F 0 "#PWR0141" H 9700 1000 50  0001 C CNN
F 1 "GND" H 9705 1077 50  0000 C CNN
F 2 "" H 9700 1250 50  0001 C CNN
F 3 "" H 9700 1250 50  0001 C CNN
	1    9700 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1250 9950 1250
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 60564CDD
P 10150 1150
F 0 "J4" H 10230 1142 50  0000 L CNN
F 1 "Conn_01x02" H 10230 1051 50  0000 L CNN
F 2 "" H 10150 1150 50  0001 C CNN
F 3 "~" H 10150 1150 50  0001 C CNN
	1    10150 1150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
